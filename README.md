<h1 align="center">Marcos Vinicius aka Marcos Derek <img src="https://media.giphy.com/media/hvRJCLFzcasrR4ia7z/giphy.gif" width="35"></h1>
<p align="center">
  <a href="https://github.com/DenverCoder1/readme-typing-svg"><img src="https://readme-typing-svg.herokuapp.com?lines=Computer+Science+Student;&center=true&width=500&height=50"></a>
</p>


<br>

<p>
<div align="center">
  <img src="https://img.shields.io/badge/-HTML-c58545?style=for-the-badge&logo=html5&logoColor=c58545&labelColor=282828">
  <img src="https://img.shields.io/badge/-CSS-d1a01f?style=for-the-badge&logo=css3&logoColor=d1a01f&labelColor=282828">
  <img src="https://img.shields.io/badge/-Python-98b982?style=for-the-badge&logo=python&logoColor=98b982&labelColor=282828">
  <img src="https://img.shields.io/badge/Java-ED8B00?style=for-the-badge&logo=java&logoColor=white">
  <img src="https://img.shields.io/badge/MySQL-00000F?style=for-the-badge&logo=mysql&logoColor=white">
  
</div>
</p>

```python
class MarcosDerek():
    
  def __init__(self):
    self.name = "Marcos Vinicius"
    self.username = "Marcos Derek"
    self.location = "DF, Brazil"
    self.web = "https://marcosderek.github.io/MarcosDerekWebsite/"
  
  def __str__(self):
    return self.name

if __name__ == '__main__':
    me = MarcosDerek()
```

------

Credit: [abhigyantrips](https://github.com/abhigyantrips)
